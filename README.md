# README #
## Automatic Overtaking System (AOS) - FEDORA team ##

# NXP Milestones #

### Milestone 1 Video: [Link Video](https://bitbucket.org/fedora/fedora-automatic-overtaking-system/src/bd5c8bb39461768bafed72041ff54eb9929c97ee/VIDEO0136.mp4?fileviewer=file-view-default) ###

### Milestone 2 Video: [Link Video](https://bitbucket.org/fedora/fedora-automatic-overtaking-system/src/1471d978742da8dd0f75edce807e8f640351e6d2/zMilestone2.mp4?fileviewer=file-view-default) ###

### Milestone 3 Video: [Link Video 1](https://bitbucket.org/fedora/fedora-automatic-overtaking-system/src/f162d3d8e643bc3f02cb9b71e6f689f70488f57e/zMilestone3Part1.mp4?fileviewer=file-view-default) [|| Link Video 2](https://bitbucket.org/fedora/fedora-automatic-overtaking-system/src/f162d3d8e643bc3f02cb9b71e6f689f70488f57e/zMilestone3Part2.mp4?fileviewer=file-view-default) ###

### Milestone 4 Video: [Link Video](https://bitbucket.org/fedora/fedora-automatic-overtaking-system/src/64ea93f5aed25a3dee7072e6ea431645cda49844/zMilestone4.mp4?at=master&fileviewer=file-view-default) ###